# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/fracatux/issues
# Bug-Submit: https://github.com/<user>/fracatux/issues/new
# Changelog: https://github.com/<user>/fracatux/blob/master/CHANGES
# Documentation: https://github.com/<user>/fracatux/wiki
# Repository-Browse: https://github.com/<user>/fracatux
# Repository: https://github.com/<user>/fracatux.git
