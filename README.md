
<a id="educajou" href="../"><img class="logo" src="https://educajou.forge.apps.education.fr/img/educajou.png"></a>

<img class="logo" style="height: 100px; width: 100px;" src="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/assets/images/logo.png">

Fracatux est un logiciel libre permettant de représenter des fractions sous forme de barres, et d'effectuer avec différentes opérations. Il permet notamment de mettre en évidence les égalités de fractions, les additions de fractions avec le même dénominateur, et la correspondance entre les écritures fractionnaires, décimale et scientifique. Une ligne graduée (décimale ou fractionnaire) permet de situer ces mêmes fractions de façon ordinale.

<a href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/paquets/fracatux_1.5.2_all.deb?ref_type=heads" target="_blank">
<span class="bouton">
Télécharger Fracatux pour Linux (DEB)
</span>
</a>

<a href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/paquets/Fracatux_installeur_v_1-5-2.exe?ref_type=heads" target="_blank">
<span class="bouton">
Télécharger Fracatux pour Windows (10 et +)
</span>
</a>

[🧐 Information importante pour l'installation sous Windows](https://educajou.forge.apps.education.fr/fracatux/#windows-10-11-64-bits)

<p style="text-align: right; color: gray; font-size: small;">
    <a href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/doc/Fracatux_Readme_PDF.pdf?inline=false" target="_blank">
        Télécharger cette documentation au format PDF
    </a>
</p>


## Aperçu

![](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/screenshot.png "Capture d'écran")

### Présentation rapide en vidéo

<div style="display: flex; align-items: center; justify-content: center;">
<iframe title="Présentation rapide de Fracatux version 1.0" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/29115e90-3780-4cfa-8c78-5110372a1202" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe> </div>

### Version annotée
<div style="text-align:center;"> <a href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/Copie_%C3%A9cran_annot%C3%A9e.jpg" target="_blank"><img width="25%" src="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/Copie_%C3%A9cran_annot%C3%A9e.jpg"></a> </div>


## Fonctionnalités

Cette application offre les fonctionnalités suivantes :

- **Représentation de n'importe quelle fraction :** vous pouvez utiliser les boutons sur la barre de gauche ou entrer n'importe quelle fraction dans l'application et les visualiser sous forme de barres. Ce qui rend plus facile leur comparaison.

- **Écriture de fractions égales par manipulation de la molette :** l'application permet également de manipuler les fractions en faisant tourner une molette pour obtenir écriture fractionnaire équivalente. Par exemple, si vous tournez la molette une fois sur la fraction de $\frac{1}{4}$, vous obtiendrez $\frac{2}{8}$, une seconde fois $\frac{3}{12}$, etc. Cela peut se faire dans les deux sens, ce qui peut permettre d'obtenir facilement des fractions irréductibles.

- **Changement d'écriture :** l'application vous permet de changer l'écriture du nombre. Vous pouvez choisir d'afficher la proportion sous forme d'une fraction, d'un nombre décimal ou en notation scientifique. Le symbole "≈" est ajouté lorsque le résultat est un arrondi avec 3 chiffres significatifs.

- **Division d'une fraction en deux fractions égales :** l'application permet de diviser une fraction en deux fractions égales en multipliant le numérateur par deux (par exemple, $\frac{5}{8}$ devient $\frac{5}{16}+\frac{5}{16}$).

- **Division en fractions unitaire :** L'application permet également de diviser une fraction en portions égales dont le numérateur est égal à $1$ (par exemple $\frac{3}{4}$ sera divisée en $\frac{1}{4} + \frac{1}{4} + \frac{1}{4}$).

- **Somme de fractions ayant le même dénominateur :** l'application peut également effectuer la somme de fractions ayant le même dénominateur.

- **Représentation/paramétrage d'une droite graduée :** en plus de la manipulation de barres de fraction, l'application permet également de représenter et de paramétrer une droite graduée. Cela peut être utile pour visualiser la position des fractions sur une droite graduée ou représenter une somme de fractions sur cette même droite.
Les graduations peuvent être décimales ou fractionnaires (par exemple, une unité sur la droite peut être divisée en $3$, $4$, $5$ ou n'importe quel nombre de graduations).

## Documentation

Des exemples d'utilisations - fiches de séance en classe

[📄CM2 Encadrer une fraction](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/doc/Fracatux_Fiche_01_CM2%20Encadrer%20une%20fraction.pdf?inline=false)

[📄CM2 Équivalence d'une fraction et d'un nombre décimal](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/doc/Fracatux_Fiche_02_CM2%20Equivalence%20fraction%20nb%20d%C3%A9cimal.pdf?inline=false)

[📄CM2 Tracer des segments de longueur donnée en fraction d'unité](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/doc/Fracatux_Fiche_03_CM2%20Segment%20exprim%C3%A9%20en%20fraction.pdf?inline=false)

[🛠️ Sur la forge : versions modifiables des documents](https://forge.apps.education.fr/educajou/fracatux/-/tree/main/doc/sources)



## Téléchargement

### Version actuelle 1.5.2

Utilisez cette version pour votre classe.

<a href="https://lstu.fr/fracatux_1_5_2_deb" target="_blank">
<span class="bouton">
Télécharger Fracatux pour Linux (DEB)
</span>
</a>

<a href="https://lstu.fr/fracatux_1_5_2_exe" target="_blank">
<span class="bouton">
Télécharger Fracatux pour Windows (10 et +)
</span>
</a>


### Ancienne version stable 1.0.1

Pour archive.

🐧 [Télécharger Fracatux pour Linux](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/paquets/fracatux_1.0.1_alt_all.deb)

Paquet DEB compatible Debian, Ubuntu (testé sur toutes versions à partir de la 20.04), Linux Mint

🪟 [Télécharger Fracatux pour Windows](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/paquets/Fracatux_installeur_v_1-0-1.exe)

Fonctionne à partir de Windows 10 inclus

----
### Versions de développement

🏗️ 🛠️ *Ces versions ne sont pas stables. Elle contiennent de nouvelles fonctionnalités mais doivent être encore testés et améliorées.
Elles peuvent comporter un certain nombre de bugs.*
*Installez ces versions si vous souhaitez participer au développement de Fracatux.*

**Version 1.5**

- amélioration du mode multiplier
- muliplication par un entier
- division par un entier
- repère de largeur de l'unité
- duplication directe par double clic
- possibilité de masquer l'écriture d'une fraction en particulier
- curseurs positionnables sur la ligne graduée
- amélioration du passage en plein écran
- amélioration de la gestion du double écran
- possibilité de dupliquer une fraction ou un groupe avant une modification
- fonction "pourcentage"
- fonction "multiplication"
- menu contextuel en cliquant droit sur une fraction
- option pour appliquer les opérations sur des copies
- bouton pour modifier le numérateur des fractions
- fonction "décomposition en facteurs premiers"
- adaptation du trait de fractions selon la longueur du texte
- adptation de la taille de police
- amélioration du rendu de l'exposant en écriure scientifique
- action de la molette de la souris opérante aussi en écriture décomposée

🐧 [Télécharger la version "dev" Linux 1.5.2 (paquet DEB)](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/paquets/fracatux_1.5.2_all.deb)

🪟 [Télécharger la version "dev" Windows 1.5.2 (EXE)](https://forge.apps.education.fr/educajou/fracatux/-/raw/main/paquets/Fracatux_installeur_v_1-5-2.exe)

## Installation

### Linux (deb : Ubuntu, Debian, Linux Mint, Primtux ...)

Sous Linux, ouvrir le paquet deb via le gestionnaire de paquets habituel (logithèque, gdebi...).

### Windows (10 & 11, 64 bits)

Exécuter le programme d'installation.

Une fenêtre Windows affiche un avertissement car le logiciel n'est pas signé avec un certificat Microsoft.

> Cliquer sur `Informations complémentaires` et `Éxécuter quand même`.
> <div> <a target="_blank" href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/InfoComp.png"><img alt="Informations complémentaires" src="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/InfoComp.png" width="25%;"></a> → <a target="_blank" href="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/Executer.png"><img alt="Executer quand même" src="https://forge.apps.education.fr/educajou/fracatux/-/raw/main/Executer.png" width="25%;"></a> </div>


## Licence
Fracatux est publié sous licence libre GNU/GPL.
Cette page de documentation est publiée sous licence Creative Commons BY SA 4.0
Auteurs : [Cyril Iaconelli](https://lmdbt.fr/), Arnaud Champollion

